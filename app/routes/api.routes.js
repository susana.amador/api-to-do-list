module.exports = app => { 
    const task = require("../controllers/task.controller");
    const user = require("../controllers/user.controller");
  
    var router = require("express").Router();
  
    /*-----Crear un nuevo Registro------ */ 
    router.post("/task", task.create);
    router.post("/user", user.create);
    router.post("/user/auth", user.signin);

    
  
    /*-----------Data List------------- */ 
    router.get("/task", task.findAll);
    router.get("/user", user.findAll);
  
  
    /*------------Search Data---------- */ 
    router.get("/task/:id", task.findOne); 
  
    /*-----------Update Data----------*/ 
    router.put("/task/:id", task.update); 
  
    /*----------Delete Data---------- */ 
    router.delete("/task/:id", task.delete);
  
    /*--------Delete all Data--------*/ 
    router.delete("/task", task.deleteAll);
  
    app.use("/api", router);
  };