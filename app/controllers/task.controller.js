const db = require("../models");
const Task = db.task_;
 
exports.create = (req, res) => { 
  if (!req.body.description) {
    res.status(400).send({ message: "Ingrese los datos solicitados para poder realizar el registro" });
    return;
  }
 
  const task = new Task({
    description: req.body.description,
    status: req.body.status
  });
 
  task
    .save(task)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Ocurrio un error al intentar crear el registro"
      });
    });
};
 
exports.findAll = (req, res) => {
  const description = req.query.description;
  var condition = description ? { description: { $regex: new RegExp(description), $options: "i" } } : {};

  Task.find(condition)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Ocurrio un error"
      });
    });
};
 
exports.findOne = (req, res) => {
  const id = req.params.id;

  Task.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "No se encontro el registro con id:" + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error al intentar realizar la busqueda del registro con id=" + id });
    });
}; 
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "El dato de actualizacion no puede ir vacio!"
    });
  }

  const id = req.params.id;

  Task.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `No se puede actualizar el registro con id=${id}. quizas el registro no existe!`
        });
      } else res.send({ message: "Actualizacion Exitosa" });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error al intentar realizar la actualizacion del registro con id=" + id
      });
    });
};
 
exports.delete = (req, res) => {
  const id = req.params.id;

  Task.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `No se puede eliminar el registro con id=${id}`
        });
      } else {
        res.send({
          message: "Registro eliminado exitosamente!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error al intentar eliminar el registro con id=" + id
      });
    });
};
 
exports.deleteAll = (req, res) => {
  Task.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Se han Eliminado todos los registros exitosamente!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Ocurrio un error al intentar eliminar todo los registros"
      });
    });
};
 
exports.findAllPublished = (req, res) => {
  Task.find({ published: true })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Ocurrio un error al intentar traer los registros."
      });
    });
};