const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors"); 

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));
 
app.use(bodyParser.json());
 
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Base de datos Conectada!");
  })
  .catch(err => {
    console.log("Error al intentar conectar base de datos", err);
    process.exit();
  });
 
app.get("/", (req, res) => {
  res.json({ 
    message: "API To Do List ", 
    CreateUser: "Method: POST  URL: 'api/user'", 
    UserList: "Method: GET  URL: 'api/user'",
    SearchUser: "Method: GET  URL: 'api/user/:id'",
    Createtask: "Method: POST  URL: 'api/task'",
    taskList: "Method: GET  URL: 'api/task'",
    Searchtask: "Method: GET  URL: 'api/task/:id'",
    Updatetask: "Method: PUT  URL: 'api/task/:id'",
    Deletetask: "Method: DELETE  URL: 'api/task/:id'",
    DeleteAlltask: "Method: DELETE  URL: 'api/task'"
 });
});

 
require("./app/routes/api.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});